
[![Build Travis](https://travis-ci.org/nayan-dev/ardupilot.svg?branch=master)](https://travis-ci.org/nayan-dev/ardupilot)

Master Branch -> Hybrid + Copter

Sparrow Branch -> Old Sparrow Code


## ArduPilot Project ##

## License ##

The ArduPilot project is licensed under the GNU General Public
License, version 3.

- [Overview of license](http://dev.ardupilot.com/wiki/license-gplv3)

- [Full Text](https://github.com/ArduPilot/ardupilot/blob/master/COPYING.txt)
